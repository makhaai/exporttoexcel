﻿using ExportExcelSample.Helpers;
using ExportToExcel.Model;
using ExportToExcel.Service;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ExportExcelSample.Controllers
{
    public class HomeController : Controller
    {
        IExcelWriterService _iExcelWriterService;

        public HomeController()
        {
            _iExcelWriterService = new ExcelWriterService();
        }
        public HomeController(IExcelWriterService iExcelWriterService)
        {
            _iExcelWriterService = iExcelWriterService;
        }

        // GET: Home
        public ActionResult Index()
        {
            var products = Data.GetProducts();

            return View(products);
        }
        public FileContentResult Download() {

            List<ExcelSheet<Product>> listOfSheets = new List<ExcelSheet<Product>>() {
                    new ExcelSheet<Product>() {
                     Records =Data.GetProducts(),
                     SheetName = "ProductList 1"
                },
                 new ExcelSheet<Product>() {
                     Records =Data.GetProducts(),
                     SheetName = "ProductList 2"
                },
            };

            var excelBytes = _iExcelWriterService.GetExcelInBytes(listOfSheets);
            var fileName = string.Format("{0}_{1}.xlsx", "Products_", DateTime.Now.ToString("MMddyyyyHHmmssfff"));
            return excelBytes != null ? File(excelBytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName) : null;
        }
    }
}