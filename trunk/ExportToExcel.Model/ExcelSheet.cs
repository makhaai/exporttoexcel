﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportToExcel.Model
{
    public class ExcelSheet<T>
    {
        public List<T> Records { get; set; }
        public string SheetName { get; set; }
    }
}
