﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using ExportToExcel.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportToExcel.Service
{
    public class ExcelWriterService : IExcelWriterService
    {
        public byte[] GetExcelInBytes<T>(IList<ExcelSheet<T>> excelWriter)
        {
            byte[] excelBytes = { };

            using (var memoryStream = new MemoryStream())
            {
                //Create a spreadsheet document in memory (by default the type is xlsx).
                using (var spreadsheetDocument = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook))
                {
                    //Add a WorkbookPart to the document.
                    var workbookpart = spreadsheetDocument.AddWorkbookPart();
                    workbookpart.Workbook = new Workbook();
                    workbookpart.Workbook.Sheets = new Sheets();
                    uint sheetId = 1;
                    var sheets = spreadsheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>();

                    foreach (ExcelSheet<T> sheet in excelWriter)
                    {
                        var headers = getHeaders(sheet.Records);

                        //Add a WorksheetPart to the WorkbookPart. 
                        var worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                        worksheetPart.Worksheet = new Worksheet();

                        //Associate the sheet data after the columns. 
                        var sheetData = new SheetData();
                        worksheetPart.Worksheet.AppendChild(sheetData);

                        //Append a new worksheet and associate it with the workbook. 
                        sheets.Append(new Sheet
                        {
                            Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart),
                            SheetId = sheetId,
                            Name = sheet.SheetName
                        });

                        //Add property name to the header row of sheet.
                        var headerRow = sheetData.AppendChild(new Row());
                        int colIdx = 0;
                        foreach (var name in headers)
                        {
                            headerRow.AppendChild(new Cell
                            {
                                CellValue = new CellValue(name),
                                DataType = CellValues.String,
                                CellReference = getColumnAddress(colIdx) + "1"
                            });
                            colIdx++;
                        }

                        //Iterate through data list collection for rows.
                        int rowIdx = 1;
                        foreach (var item in sheet.Records)
                        {
                            var contentRow = sheetData.AppendChild(new Row());
                            colIdx = 0;
                            //Iterate through property collection for columns.
                            foreach (var prop in typeof(T).GetProperties())
                            {
                                //Do property value.                
                                var value = prop.GetValue(item, null);

                                //Assign value to cell
                                contentRow.AppendChild(new Cell
                                {
                                    CellValue = new CellValue(value != null ? value.ToString() : null),
                                    DataType = CellValues.String,
                                    CellReference = getColumnAddress(colIdx) + (rowIdx + 1).ToString()
                                });

                                colIdx++;
                            }
                            rowIdx++;
                        }
                        workbookpart.Workbook.Save();
                        sheetId++;
                    }
                    //Convert memory stream to byte array
                }
                excelBytes = memoryStream.ToArray();
            }

            return excelBytes;
        }
        private String getColumnAddress(int columnIndex)
        {
            Stack<char> stack = new Stack<char>();
            while (columnIndex >= 0)
            {
                stack.Push((char)('A' + (columnIndex % 26)));
                columnIndex = (columnIndex / 26) - 1;
            }
            return new String(stack.ToArray());
        }
        private List<string> getHeaders<T>(List<T> list)
        {
            List<string> headers = new List<string>();
            headers.AddRange(typeof(T).GetProperties().Select(x => x.Name));
            return headers;
        }
    }
}
