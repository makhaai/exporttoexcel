﻿using ExportToExcel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportToExcel.Service
{
    public interface IExcelWriterService
    {
        byte[] GetExcelInBytes<T>(IList<ExcelSheet<T>> excelWriter);
    }
}
